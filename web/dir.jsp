<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Insert title here</title>

    <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    <script src="resources/js/jquery-3.2.1.slim.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
</head>
<body>
<p>
    <%--@elvariable id="fileList" type="java.util.List"--%>


</p>
<table class="table table-striped">
    <thead>
    <tr>
        <td>
            <b>Name</b>
        </td>
        <td>
            <b>Type</b>
        </td>
        <td>
            <b>Size</b>
        </td>
        <td>
            <b>Create date</b>
        </td>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="list" items="${fileList}">
        <tr>
            <td>
                <a href="<c:out value="${list.getFilePath()}"/>"><c:out value="${list.getFileName()}"/></a>
            </td>
            <td>
                <c:out value="${list.getFileType()}"/>
            </td>
            <td>
                <c:out value="${list.getSize()}"/>
            </td>
            <td>
                <c:out value="${list.getDateCreate()}"/>
            </td>
        </tr>
    </c:forEach>

    </tbody>
</table>

</body>
</html>