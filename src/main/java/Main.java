import dao.FileDAO;
import dao.FileDAOImpl;

import dto.FileDTO;
import dto.OpenResultDTO;
import opener.Opener;
import opener.OpenerFolderImpl;
import opener.OpenerTextImpl;
import service.FileService;
import service.FileServiceImpl;
import service.TextService;
import service.TextServiceImpl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        FileDAO fileDAO = new FileDAOImpl();
        List<Opener> openerList = new ArrayList<>();
        openerList.add(new OpenerFolderImpl());
        openerList.add(new OpenerTextImpl());

        TextService textService = new TextServiceImpl();
        FileService fileService = new FileServiceImpl(fileDAO, openerList);
        File path = new File("D:/java/test/");
        File path2 = new File("D://java/test//s.txt");
        File path3 = new File("D://java/test//text.txt");

        System.out.println("===OpenerFolder===");
        OpenResultDTO open = fileService.open(path);
        List<FileDTO> value = (List<FileDTO>) open.getValue();
        value.forEach(System.out::println);

        System.out.println("===OpenerText===");
        System.out.println(fileService.open(path2));

        System.out.println("===Save===");
        System.out.println(textService.saveText(path3, "Hello world!"));
    }
}
