package opener;

import dto.FileDTO;
import dto.OpenResultDTO;
import utils.FileUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;


public class OpenerFolderImpl implements Opener {

    @Override
    public String getOpenerType() {
        return "dir";
    }

    @Override
    public OpenResultDTO openerTarget(File path) {
        List<FileDTO> listFileDTO = new ArrayList<>();

        List<File> listFilesAndDirs = Arrays.asList(
                Objects.requireNonNull(path.listFiles()));

        listFilesAndDirs.forEach(
                file -> listFileDTO.add(new FileDTO(
                        file.getName(), FileUtil.getFileExtension(file),
                        file.getPath(), FileUtil.getSize(file), FileUtil.lastModifierFileDate(file))));

        return new OpenResultDTO("dirHandler", listFileDTO, new File(path.getAbsolutePath()));
    }
}
