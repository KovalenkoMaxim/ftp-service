package opener;

import dto.OpenResultDTO;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class OpenerTextImpl implements Opener{

    @Override
    public String getOpenerType() {
        return "txt";
    }

    @Override
    public OpenResultDTO openerTarget(File path) {
        String readTheText = "";
        try {
            readTheText = FileUtils.readFileToString(
                    new File(String.valueOf(path)), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new OpenResultDTO("txtHandler", readTheText, new File(path.getParent()));
    }
}
