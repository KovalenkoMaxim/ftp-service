package opener;

import dto.OpenResultDTO;

import java.io.File;

public interface Opener {
    String getOpenerType();

    OpenResultDTO openerTarget(File path);
}
