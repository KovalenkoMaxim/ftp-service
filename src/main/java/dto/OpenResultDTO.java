package dto;

import lombok.Data;

import java.io.File;

@Data
public class OpenResultDTO<T> {
    private String visualHandler;
    private T value;
    private File currentPath;

    public OpenResultDTO(String visualHandler, T value, File currentPath) {
        this.visualHandler = visualHandler;
        this.value = value;
        this.currentPath = currentPath;
    }
}
