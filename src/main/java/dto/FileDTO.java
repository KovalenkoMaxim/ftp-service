package dto;

import lombok.Data;

import java.util.Date;

@Data
public class FileDTO {
    private String fileName;
    private String fileType;
    private String filePath;
    private String size;
    private Date dateCreate;

    public FileDTO(String fileName, String fileType, String filePath, String size, Date dateCreate) {
        this.fileName = fileName;
        this.fileType = fileType;
        this.filePath = filePath;
        this.size = size;
        this.dateCreate = dateCreate;
    }
}
