package service;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class TextServiceImpl implements TextService {

    @Override
    public boolean saveText(File path, String data) {
        boolean isSave = false;
        try {
            FileUtils.writeStringToFile(path, data, StandardCharsets.UTF_8, true);
            isSave = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isSave;
    }
}
