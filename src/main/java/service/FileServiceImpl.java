package service;

import dao.FileDAO;
import dto.OpenResultDTO;
import opener.Opener;
import utils.FileUtil;

import java.io.File;
import java.util.List;
import java.util.Optional;

public class FileServiceImpl implements FileService {
    private final FileDAO fileDAO;
    private final List<Opener> openerList;

    public FileServiceImpl(FileDAO fileDAO, List<Opener> openerList) {
        this.fileDAO = fileDAO;
        this.openerList = openerList;
    }

    @Override
    public boolean create(File path, String fileName, boolean isFolder) {
        boolean isCreate = false;

        File file = FileUtil.linkPathAndFileName(path, fileName);
        if (!file.exists()) {
            isCreate = fileDAO.create(path, fileName, isFolder);
        }
        return isCreate;
    }

    @Override
    public boolean delete(File path) {
        boolean isDelete = false;

        if (path.exists()) {
            isDelete = fileDAO.delete(path);
        }
        return isDelete;
    }

    @Override
    public OpenResultDTO open(File path) {
        Optional<Opener> optionalOpener = Optional.ofNullable(openerList.stream()
                .filter(opener ->
                        opener.getOpenerType()
                                .equalsIgnoreCase(FileUtil.getFileExtension(path)))
                .findFirst().orElseThrow(NullPointerException::new));

        return optionalOpener.get().openerTarget(path);
    }


}
