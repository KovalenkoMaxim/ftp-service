package service;

import java.io.File;

public interface TextService {

    boolean saveText(File path, String data);
}
