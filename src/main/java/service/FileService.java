package service;

import dto.OpenResultDTO;

import java.io.File;

public interface FileService {

    boolean create(File path, String fileName, boolean isFolder);

    boolean delete(File path);

    OpenResultDTO open(File path);


}
