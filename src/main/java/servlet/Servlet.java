package servlet;

import dao.FileDAOImpl;
import dto.FileDTO;
import dto.OpenResultDTO;
import opener.Opener;
import opener.OpenerFolderImpl;
import opener.OpenerTextImpl;
import service.FileService;
import service.FileServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@WebServlet("/dir")
public class Servlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Opener> openerList = new ArrayList<>();
        openerList.add(new OpenerFolderImpl());
        openerList.add(new OpenerTextImpl());
        FileService fileService = new FileServiceImpl(new FileDAOImpl(), openerList);
        File path = new File("D:/java/test/");
        OpenResultDTO open = fileService.open(path);
        List<FileDTO> list = (List<FileDTO>) open.getValue();

        resp.setContentType("text/html");

        req.setAttribute("fileList", list);

        getServletContext().getRequestDispatcher("/dir.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }

    @Override
    public void init() throws ServletException {
        super.init();
    }
}
