package dao;

import java.io.File;

public interface FileDAO {

    boolean create(File path, String fileName, boolean isFolder);

    boolean delete(File path);

}
