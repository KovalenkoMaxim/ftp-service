package dao;

import org.apache.commons.io.FileUtils;
import utils.FileUtil;

import java.io.*;

public class FileDAOImpl implements FileDAO {

    @Override
    public boolean create(File path, String fileName, boolean isFolder) {
        boolean isFile = false;

        if ((!path.exists() &&
                fileName.lastIndexOf(".") != -1
                && !isFolder)) {
            try {
                isFile = FileUtil.linkPathAndFileName(
                        path, fileName).createNewFile();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (isFolder) {
            isFile = FileUtil.linkPathAndFileName(
                    path, fileName).mkdir();
        }

        return isFile;
    }

    @Override
    public boolean delete(File path) {
        boolean isDelete = false;
        if (path.exists() && path.isDirectory()) {
            try {
                FileUtils.deleteDirectory(path);
                isDelete = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (path.exists() && path.isFile()) {
            try {
                FileUtils.forceDelete(path);
                isDelete = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return isDelete;
    }
}
