package utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;

public class FileUtil {

    public static String getSize(File file) {
        String stringSize = "";
        long size;

        size = FileUtils.sizeOf(file);
        if (size <= 1024) {
            stringSize = size + "byte";
        } else if (size < 1048586) {
            stringSize = size / 1024 + "kb";
        } else if (size < 1073741824) {
            stringSize = size / 1048586 + "mb";
        }

        if (file.isDirectory() && file.length() == 0) {
            stringSize = "";
        }

        return stringSize;
    }

    public static String getFileExtension(File file) {
        String name = file.getName();
        if (file.isFile() && name.lastIndexOf(".") != -1
                && name.lastIndexOf(".") != 0) {
            return name.substring(name.lastIndexOf(".") + 1);
        } else if (file.isFile()) {
            return "NonExtension";
        } else
            return "dir";
    }

    public static File linkPathAndFileName(File path, String fileName) {

        return new File(path, fileName);
    }

    public static Date lastModifierFileDate(File path) {

        return (new Date(path.lastModified()));
    }
}
